package com.onlinestore.onlinestore.exception;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.ServletWebRequest;
import org.springframework.web.context.request.WebRequest;

import javax.persistence.NonUniqueResultException;
import java.time.OffsetDateTime;

import static com.onlinestore.onlinestore.exception.ErrorCode.*;

@RestControllerAdvice
@Slf4j
public class ExceptionHandler {

    @org.springframework.web.bind.annotation.ExceptionHandler(CannotViewYourProfile.class)
    public ResponseEntity<ErrorResponseDto> handleCannotViewYourProfile(CannotViewYourProfile ex,
                                                                                   WebRequest webRequest) {
        String language = webRequest.getHeader("Accept-Language");
        log.info("Request language is {}", language);
        var path = ((ServletWebRequest) webRequest).getRequest().getRequestURL().toString();
        log.error("Exception", ex.getLocalizedMessage());
        ex.printStackTrace();
        return ResponseEntity.status(400).body(ErrorResponseDto.builder()
                .status(400)
                .code(MS9_BAD_REQUEST_001.toString())
                .message("Bad request")
                .detail("You cannot view your profile")
                .timestamp(OffsetDateTime.now())
                .path(path)
                .build());

    }

    @org.springframework.web.bind.annotation.ExceptionHandler(UsernameAndPasswordNotNull.class)
    public ResponseEntity<ErrorResponseDto> handleUsernameAndPasswordNotNull(UsernameAndPasswordNotNull ex,
                                                                                   WebRequest webRequest) {
        String language = webRequest.getHeader("Accept-Language");
        log.info("Request language is {}", language);
        var path = ((ServletWebRequest) webRequest).getRequest().getRequestURL().toString();
        log.error("Exception", ex.getLocalizedMessage());
        ex.printStackTrace();
        return ResponseEntity.status(400).body(ErrorResponseDto.builder()
                .status(400)
                .code(MS9_BAD_REQUEST_002.toString())
                .message("Bad request")
                .detail("Username password or confirm password not null ")
                .timestamp(OffsetDateTime.now())
                .path(path)
                .build());

    }

    @org.springframework.web.bind.annotation.ExceptionHandler(NonUniqueResultException.class)
    public ResponseEntity<ErrorResponseDto> handleEmailUniqueException(NonUniqueResultException ex,
                                                                                   WebRequest webRequest) {
        String language = webRequest.getHeader("Accept-Language");
        log.info("Request language is {}", language);
        var path = ((ServletWebRequest) webRequest).getRequest().getRequestURL().toString();
        log.error("Exception", ex.getLocalizedMessage());
        ex.printStackTrace();
        return ResponseEntity.status(400).body(ErrorResponseDto.builder()
                .status(400)
                .code(MS9_BAD_REQUEST_003.toString())
                .message("Bad request")
                .detail("Bu Email mövcuddur ")
                .timestamp(OffsetDateTime.now())
                .path(path)
                .build());

    }

    @org.springframework.web.bind.annotation.ExceptionHandler(ConfirmPasswordEqualsPass.class)
    public ResponseEntity<ErrorResponseDto> handleConfirmPasswordEqualPass(ConfirmPasswordEqualsPass ex,
                                                                       WebRequest webRequest) {
        String language = webRequest.getHeader("Accept-Language");
        log.info("Request language is {}", language);
        var path = ((ServletWebRequest) webRequest).getRequest().getRequestURL().toString();
        log.error("Exception", ex.getLocalizedMessage());
        ex.printStackTrace();
        return ResponseEntity.status(400).body(ErrorResponseDto.builder()
                .status(400)
                .code(MS9_BAD_REQUEST_004.toString())
                .message("Bad request")
                .detail("Parol Eyni olmalidir ")
                .timestamp(OffsetDateTime.now())
                .path(path)
                .build());

    }


}