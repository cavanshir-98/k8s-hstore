package com.onlinestore.onlinestore.dto;

import lombok.Data;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.Size;

@Data
public class RegistrationDto {
    private String email;

    @Size(min = 6,max = 10)
    private String pass;

    @Size(min = 6,max = 10)
    private String conPass;
}
